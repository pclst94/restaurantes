<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoExtra extends Model 
{

    protected $table = 'productoExtra';
    public $timestamps = true;

    public function getTipoProducto()
    {
        return $this->belongsTo('TipoProducto', 'idTipoDeProducto');
    }

    public function getProductoPadre()
    {
        return $this->belongsTo('ProductoExtra', 'idProductoPadre');
    }

    public function getVariaciones()
    {
        return $this->hasMany('ProductVariation');
    }

    public function getHorario()
    {
        return $this->hasOne('Horarios', 'idHorario');
    }

    public function getFechasHabilitacion()
    {
        return $this->hasOne('FechasHabilitacion', 'idFechasHabilitacion');
    }

}