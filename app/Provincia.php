<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model 
{

    protected $table = 'provincia';
    public $timestamps = true;

    public function getDistritos()
    {
        return $this->hasMany('Distrito');
    }

}