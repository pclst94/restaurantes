<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiasFestivos extends Model 
{

    protected $table = 'diasFestivos';
    public $timestamps = true;

    public function getHorarios()
    {
        return $this->hasMany('Horarios');
    }

}