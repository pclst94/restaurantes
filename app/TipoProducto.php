<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProducto extends Model 
{

    protected $table = 'tipoProducto';
    public $timestamps = true;

    public function getProductos()
    {
        return $this->hasMany('ProductoExtra');
    }

}