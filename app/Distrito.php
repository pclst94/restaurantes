<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model 
{

    protected $table = 'distrito';
    public $timestamps = true;
    protected $fillable = array('idProvincia');
    protected $visible = array('idProvincia');

    public function getSucursales()
    {
        return $this->hasMany('Sucursal');
    }

}