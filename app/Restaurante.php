<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurante extends Model 
{

    protected $table = 'restaurante';
    public $timestamps = true;

    public function getTiposproductos()
    {
        return $this->hasMany('TipoProducto');
    }

    public function getSucursales()
    {
        return $this->hasMany('Sucursal');
    }

    public function getComentariosRating()
    {
        return $this->hasMany('ComentariosRating');
    }

    public function getCategoriaEspecialidad()
    {
        return $this->belongsToMany('CategoriaEspecialidad');
    }

    public function getHorario()
    {
        return $this->belongsTo('Horarios', 'idHorario');
    }

}