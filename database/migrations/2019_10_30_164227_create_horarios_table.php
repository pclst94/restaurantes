<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorariosTable extends Migration {

	public function up()
	{
		Schema::create('horarios', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idDiafestivo')->unsigned();
			$table->timestamp('lunes')->nullable();
			$table->timestamp('martes')->nullable();
			$table->timestamp('miercoles')->nullable();
			$table->timestamp('jueves')->nullable();
			$table->timestamp('viernes')->nullable();
			$table->timestamp('sabado')->nullable();
			$table->timestamp('domingo')->nullable();
			$table->timestamp('feriados')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('horarios');
	}
}