<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestauranteTable extends Migration {

	public function up()
	{
		Schema::create('restaurante', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idHorario')->unsigned();
			$table->string('razonSocial');
			$table->string('Ruc');
			$table->string('Telefono');
			$table->double('montoMinimoPedido');
			$table->double('ratioDeliveryCostoKilometro');
			$table->double('costoMinimoDelivery');
			$table->string('direccion');
			$table->string('descripcionRestaurante');
			$table->integer('tipoServicioDelivery');
			$table->string('metodosPago');
			$table->double('ratingPromedioEstrellas');
			$table->longText('imagenPortada');
			$table->longText('logotipoRestaurante');
			$table->boolean('estadoHabilitado');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('restaurante');
	}
}