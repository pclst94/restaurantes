<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductVariationTable extends Migration {

	public function up()
	{
		Schema::create('productVariation', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idProductoExtra')->unsigned();
			$table->string('titulo');
			$table->double('precio');
			$table->string('sku');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('productVariation');
	}
}