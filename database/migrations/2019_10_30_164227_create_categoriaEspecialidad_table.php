<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriaEspecialidadTable extends Migration {

	public function up()
	{
		Schema::create('categoriaEspecialidad', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idCatPadre')->unsigned();
			$table->string('nombre');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('categoriaEspecialidad');
	}
}