<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDistritoTable extends Migration {

	public function up()
	{
		Schema::create('distrito', function(Blueprint $table) {
			$table->increments('id', true)->primary();
			$table->integer('idProvincia')->unsigned();
			$table->string('nombre');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('distrito');
	}
}