<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiasFestivosTable extends Migration {

	public function up()
	{
		Schema::create('diasFestivos', function(Blueprint $table) {
			$table->increments('id', true)->primary();
			$table->string('fecha');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('diasFestivos');
	}
}