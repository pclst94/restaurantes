<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSucursalTable extends Migration {

	public function up()
	{
		Schema::create('sucursal', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idRestaurante')->unsigned();
			$table->integer('idDistrito')->unsigned();
			$table->string('nombreSucursal');
			$table->string('direccion');
			$table->string('telefono');
			$table->string('coordenadas');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sucursal');
	}
}