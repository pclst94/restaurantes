<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductoExtraTable extends Migration {

	public function up()
	{
		Schema::create('productoExtra', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idProductoPadre')->unsigned()->nullable();
			$table->integer('idHorario')->unsigned();
			$table->integer('idTipoDeProducto')->unsigned();
			$table->string('titulo');
			$table->string('descripcion');
			$table->longText('imagen');
			$table->string('horariosFechasApertura');
			$table->integer('limiteExtra');
			$table->integer('isOfferOrProduct');
			$table->boolean('isExclusiveOffer');
			$table->timestamps();
			$table->integer('idFechasHabilitacion')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('productoExtra');
	}
}