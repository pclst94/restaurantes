<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorariosTable extends Migration {

	public function up()
	{
		Schema::create('horarios', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idDiafestivo')->unsigned();
			$table->timestamp('lunes');
			$table->timestamp('martes');
			$table->timestamp('miercoles');
			$table->timestamps();
			$table->timestamps();
			$table->timestamps();
			$table->timestamps();
			$table->timestamps();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('horarios');
	}
}