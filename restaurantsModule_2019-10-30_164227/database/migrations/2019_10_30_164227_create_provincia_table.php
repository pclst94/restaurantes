<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProvinciaTable extends Migration {

	public function up()
	{
		Schema::create('provincia', function(Blueprint $table) {
			$table->increments('id', true)->primary();
			$table->string('nombre');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('provincia');
	}
}