<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FechasHabilitacion extends Model 
{

    protected $table = 'fechasHabilitacion';
    public $timestamps = true;

}