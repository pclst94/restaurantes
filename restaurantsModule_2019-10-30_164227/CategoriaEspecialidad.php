<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaEspecialidad extends Model 
{

    protected $table = 'categoriaEspecialidad';
    public $timestamps = true;

    public function getCategoriasHijo()
    {
        return $this->hasMany('CategoriaEspecialidad');
    }

    public function getCategoriaPadre()
    {
        return $this->belongsTo('CategoriaEspecialidad', 'idCatPadre');
    }

    public function getRestaurantes()
    {
        return $this->hasMany('Restaurante');
    }

}