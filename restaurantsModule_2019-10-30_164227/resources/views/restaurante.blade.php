{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idHorario', 'IdHorario:') !!}
			{!! Form::text('idHorario') !!}
		</li>
		<li>
			{!! Form::label('razonSocial', 'RazonSocial:') !!}
			{!! Form::text('razonSocial') !!}
		</li>
		<li>
			{!! Form::label('Ruc', 'Ruc:') !!}
			{!! Form::text('Ruc') !!}
		</li>
		<li>
			{!! Form::label('Telefono', 'Telefono:') !!}
			{!! Form::text('Telefono') !!}
		</li>
		<li>
			{!! Form::label('montoMinimoPedido', 'MontoMinimoPedido:') !!}
			{!! Form::text('montoMinimoPedido') !!}
		</li>
		<li>
			{!! Form::label('ratioDeliveryCostoKilometro', 'RatioDeliveryCostoKilometro:') !!}
			{!! Form::text('ratioDeliveryCostoKilometro') !!}
		</li>
		<li>
			{!! Form::label('costoMinimoDelivery', 'CostoMinimoDelivery:') !!}
			{!! Form::text('costoMinimoDelivery') !!}
		</li>
		<li>
			{!! Form::label('direccion', 'Direccion:') !!}
			{!! Form::text('direccion') !!}
		</li>
		<li>
			{!! Form::label('descripcionRestaurante', 'DescripcionRestaurante:') !!}
			{!! Form::text('descripcionRestaurante') !!}
		</li>
		<li>
			{!! Form::label('tipoServicioDelivery', 'TipoServicioDelivery:') !!}
			{!! Form::text('tipoServicioDelivery') !!}
		</li>
		<li>
			{!! Form::label('metodosPago', 'MetodosPago:') !!}
			{!! Form::text('metodosPago') !!}
		</li>
		<li>
			{!! Form::label('ratingPromedioEstrellas', 'RatingPromedioEstrellas:') !!}
			{!! Form::text('ratingPromedioEstrellas') !!}
		</li>
		<li>
			{!! Form::label('imagenPortada', 'ImagenPortada:') !!}
			{!! Form::text('imagenPortada') !!}
		</li>
		<li>
			{!! Form::label('logotipoRestaurante', 'LogotipoRestaurante:') !!}
			{!! Form::text('logotipoRestaurante') !!}
		</li>
		<li>
			{!! Form::label('estadoHabilitado', 'EstadoHabilitado:') !!}
			{!! Form::text('estadoHabilitado') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}