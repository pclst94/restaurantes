<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model 
{

    protected $table = 'productVariation';
    public $timestamps = true;

    public function getProductExtra()
    {
        return $this->belongsTo('ProductoExtra', 'idProductoExtra');
    }

}