{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idRestaurante', 'IdRestaurante:') !!}
			{!! Form::text('idRestaurante') !!}
		</li>
		<li>
			{!! Form::label('comentario', 'Comentario:') !!}
			{!! Form::text('comentario') !!}
		</li>
		<li>
			{!! Form::label('puntuacion', 'Puntuacion:') !!}
			{!! Form::text('puntuacion') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}