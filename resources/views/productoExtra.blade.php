{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idProductoPadre', 'IdProductoPadre:') !!}
			{!! Form::text('idProductoPadre') !!}
		</li>
		<li>
			{!! Form::label('idHorario', 'IdHorario:') !!}
			{!! Form::text('idHorario') !!}
		</li>
		<li>
			{!! Form::label('idTipoDeProducto', 'IdTipoDeProducto:') !!}
			{!! Form::text('idTipoDeProducto') !!}
		</li>
		<li>
			{!! Form::label('titulo', 'Titulo:') !!}
			{!! Form::text('titulo') !!}
		</li>
		<li>
			{!! Form::label('descripcion', 'Descripcion:') !!}
			{!! Form::text('descripcion') !!}
		</li>
		<li>
			{!! Form::label('imagen', 'Imagen:') !!}
			{!! Form::text('imagen') !!}
		</li>
		<li>
			{!! Form::label('horariosFechasApertura', 'HorariosFechasApertura:') !!}
			{!! Form::text('horariosFechasApertura') !!}
		</li>
		<li>
			{!! Form::label('limiteExtra', 'LimiteExtra:') !!}
			{!! Form::text('limiteExtra') !!}
		</li>
		<li>
			{!! Form::label('isOfferOrProduct', 'IsOfferOrProduct:') !!}
			{!! Form::text('isOfferOrProduct') !!}
		</li>
		<li>
			{!! Form::label('isExclusiveOffer', 'IsExclusiveOffer:') !!}
			{!! Form::text('isExclusiveOffer') !!}
		</li>
		<li>
			{!! Form::label('idFechasHabilitacion', 'IdFechasHabilitacion:') !!}
			{!! Form::text('idFechasHabilitacion') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}