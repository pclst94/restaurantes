{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idRestaurante', 'IdRestaurante:') !!}
			{!! Form::text('idRestaurante') !!}
		</li>
		<li>
			{!! Form::label('idDistrito', 'IdDistrito:') !!}
			{!! Form::text('idDistrito') !!}
		</li>
		<li>
			{!! Form::label('nombreSucursal', 'NombreSucursal:') !!}
			{!! Form::text('nombreSucursal') !!}
		</li>
		<li>
			{!! Form::label('direccion', 'Direccion:') !!}
			{!! Form::text('direccion') !!}
		</li>
		<li>
			{!! Form::label('telefono', 'Telefono:') !!}
			{!! Form::text('telefono') !!}
		</li>
		<li>
			{!! Form::label('coordenadas', 'Coordenadas:') !!}
			{!! Form::text('coordenadas') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}