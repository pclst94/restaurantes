{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idProductoExtra', 'IdProductoExtra:') !!}
			{!! Form::text('idProductoExtra') !!}
		</li>
		<li>
			{!! Form::label('titulo', 'Titulo:') !!}
			{!! Form::text('titulo') !!}
		</li>
		<li>
			{!! Form::label('precio', 'Precio:') !!}
			{!! Form::text('precio') !!}
		</li>
		<li>
			{!! Form::label('sku', 'Sku:') !!}
			{!! Form::text('sku') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}