{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('fechaInicio', 'FechaInicio:') !!}
			{!! Form::text('fechaInicio') !!}
		</li>
		<li>
			{!! Form::label('fechaFinal', 'FechaFinal:') !!}
			{!! Form::text('fechaFinal') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}