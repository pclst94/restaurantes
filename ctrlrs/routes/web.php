<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('restaurante', 'RestauranteController');
Route::resource('sucursal', 'SucursalController');
Route::resource('distrito', 'DistritoController');
Route::resource('categoriaespecialidad', 'CategoriaEspecialidadController');
Route::resource('comentariosrating', 'ComentariosRatingController');
Route::resource('productoextra', 'ProductoExtraController');
Route::resource('productvariation', 'ProductVariationController');
Route::resource('tipoproducto', 'TipoProductoController');
Route::resource('provincia', 'ProvinciaController');
Route::resource('horarios', 'HorariosController');
Route::resource('diasfestivos', 'DiasFestivosController');
Route::resource('fechashabilitacion', 'FechasHabilitacionController');
